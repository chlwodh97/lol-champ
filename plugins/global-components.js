import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import lolChampInfo from '~/components/lol-champ-info'
Vue.component('lolChampInfo', lolChampInfo)
